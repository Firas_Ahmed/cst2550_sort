#ifndef _SORT_H_
#define _SORT_H_

#include <vector>
#include <algorithm>

/*
  Use mergesort algorithm to sort a vector of integers.  Works by
  recursively sorting the two halves of the input vector and then
  merging the two sorted halves back together.
  @param list the list of integers to be sorted
*/
void mergesort(std::vector<int> &list);

/*
  Merge two sorted std::vectors of integers into a destination vector.
  @param list1 a list of sorted integers to merge 
  @param list2 a list of sorted integers to merge 
  @param destination the vector to merge the two sorted vectors into,
         the capacity must be sufficient to hold the elements of both
         sorted vectors.
*/
void merge(std::vector<int> &list1, std::vector<int> &list2,
	   std::vector<int> &destination);

int partition(std::vector<int> &A, int lo, int hi);

void quickSort(std::vector<int> &A, int lo, int hi);

#endif
